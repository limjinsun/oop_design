import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public class Depot implements DepotBehavior {

    static AtomicInteger nextId = new AtomicInteger();
    private int depotId;
    private Product nativeProduct;
    private int minNative;
    private int maxNative;
    private int nativeCount;
    private int minForeign;
    private int maxForeign;
    private int foreignCount;
    private BigDecimal cash;
    private BigDecimal costOfProduct;
    private BigDecimal costOfDelivery;
    private BigDecimal totalCost;
    EnumMap<Product, Integer> productCountsMap;

    public Depot(Product nativeProduct, int minNative, int maxNative, int minForeign, int maxForeign, BigDecimal costOfProduct, BigDecimal costOfDelivery, BigDecimal cash) {
        this.depotId = nextId.incrementAndGet();
        this.nativeProduct = nativeProduct;
        this.minNative = minNative;
        this.maxNative = maxNative;
        int randomNativeCount = ThreadLocalRandom.current().nextInt(minNative, maxNative + 1);
        this.minForeign = minForeign;
        this.maxForeign = maxForeign;
        this.costOfProduct = round(costOfProduct);
        this.costOfDelivery = round(costOfDelivery);
        this.cash = round(cash);
        BigDecimal b = costOfProduct;
        this.totalCost = round(b.add(costOfDelivery));
        this.productCountsMap = new EnumMap<>(Product.class);
        for (Product product : Product.values()) {
            if (nativeProduct == product) {
                productCountsMap.put(product, randomNativeCount);
            } else {
                productCountsMap.put(product, 0);
            }
        }
        this.nativeCount = productCountsMap.get(nativeProduct);
        this.foreignCount = getForeignProductSum(nativeProduct, this.productCountsMap); // see the DepotBehavior interface
    }



    /* getter and setters */

    public Product getNativeProduct() {
        return nativeProduct;
    }

    public void setForeignCount(int foreignCount) {
        this.foreignCount = foreignCount;
    }

    public void setNativeCount(int nativeCount) {
        this.nativeCount = nativeCount;
    }

    public int getNativeCount() {
        return nativeCount;
    }

    public int getForeignCount() {
        return foreignCount;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public String toString() {
        return "--- Depot info ---\n" + "Depot-id : " + depotId + "\nNative-product : " + nativeProduct + "\ncash : " + this.cash + "\ncost of Product : " + this.costOfProduct + "\ncost of Delivery : " + this.costOfDelivery + "\ntotalCost : " + this.totalCost + "\nnative product counts : " + this.nativeCount + "\nforeign product counts : " + this.foreignCount;
    }
}