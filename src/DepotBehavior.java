import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.EnumMap;

public interface DepotBehavior {
    /* rounding BigDecimal Value */
    public default BigDecimal round(BigDecimal b) {
        return b.setScale(2, RoundingMode.CEILING);
    }

    /* get sum of Foreign item in productCountsMap */
    public default int getForeignProductSum(Product product, EnumMap<Product, Integer> productCountsMap ) {
        int sum = 0;
        for (EnumMap.Entry<Product, Integer> entry : productCountsMap.entrySet()) {
            if (entry.getKey() != product) {
                sum += entry.getValue();
            }
        }
        return sum;
    }
}
