import java.math.BigDecimal;

public class test {

    public static void main(String args[]) {

        Company A = new Company("BigA",Product.A,100,15,50,3,40);
        Company B = new Company("BigB",Product.B,100,15,50,3,40);
        Company C = new Company("BigC",Product.C,100,15,50,3,40);

        System.out.println("\n\n----** Before Trade **----\n\n" );

        System.out.println(A.depot[9]);
        System.out.println(B.depot[99]);


        System.out.println("\n--------" );
        System.out.println(A.depot[9].productCountsMap.entrySet());
        System.out.println(B.depot[99].productCountsMap.entrySet());

        TradingManager manager = new TradingManager();
        if (manager.checkTradeAvailable(A.depot[9], B.depot[99])){
            manager.doTrade(A.depot[9], B.depot[99]);
        }

        System.out.println("\n\n----** After Trade **----\n\n" );

        System.out.println(A.depot[9]);
        System.out.println(B.depot[99]);

        System.out.println("\n--------" );
        System.out.println(A.depot[9].productCountsMap.entrySet());
        System.out.println(B.depot[99].productCountsMap.entrySet());
    }

}