import java.math.BigDecimal;

public class Company implements CompanyBehavior {
    private String companyName;
    protected Depot depot[];

    public Company (String name, Product nativeProduct, int depotCount, int minNative, int maxNative, int minForeign, int maxForeign){
        this.companyName = name;
        this.depot = new Depot[depotCount];

        for (int i=0; i<depotCount; i++){
            BigDecimal costOfProduct = randomValue("10.00","1.00");
            BigDecimal costOfDelivery = randomValue("10.00","1.00");
            BigDecimal cash = randomValue("100.00","50.00");
            depot[i] = new Depot(nativeProduct,minNative,maxNative,minForeign,maxForeign,costOfProduct,costOfDelivery,cash);
        }
    }

    private BigDecimal randomValue(String M, String m) {
        BigDecimal max = new BigDecimal(M);
        BigDecimal min = new BigDecimal(m);
        BigDecimal range = max.subtract(min);
        BigDecimal result = min.add(range.multiply(new BigDecimal(Math.random())));
        return result;
    }

    public void calculatePurchaseCost(){};
    public void calculateDeliveryCost(){};
    public void tradeInfo(){};
    public void purchaseAmount(){};

    public String getCompanyName() {
        return companyName;
    }

}