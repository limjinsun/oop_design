/*

depots each containing a varying number of Native Product's (min stock of A's = 15)(max stock 50)

A user must be able to select which company to trade as
A user must be able to select to allow the program to trade autonomously

Every depot has a cash allowance minimum 50 maximum 100

A depot cannot go below its minimum stock of its native product
A depot cannot store above its maximum stock of its native product
- min stock 15
- max stock 50

A depot cannot go below its minimum stock of its purchase products
A depot cannot store above its maximum stock of its purchase products
- minimum of 3
- maximum of 40

All depots must attempt to trade with every applicable depot (every ‘A’ depot must attempt to trade
with every ‘B’ and ‘C’ depot etc.)

Product prices and delivery prices are random numbers between 1 & 10

*/

// trade keep doing till every depot's foreign stock higher than 3.

/*

            buyer's cash has to be > productPrice
            buyer's cash has to be  > 50 + productPrice.
            Seller's cash has to be < 100 - productPrice.

            Seller's native stock should be > 15 + 1
            Buyer's foreign stock should be < 40

            buyer.getCash().compareTo(productPrice); // has to be 1. not 0 or -1
            buyer.getCash().compareTo(productPrice.add(new BigDecimal("50"))); // has to be 1. not 0 or -1
            seller.getCash().compareTo(new BigDecimal("100").subtract(productPrice)); //  has to be -1. not 0 or 1

            seller.getNativeCount() > 16
            buyer.getForeignCount() < 40

*/

import java.math.BigDecimal;


public class TradingManager implements DepotBehavior {
    public TradingManager() {}
    Company[] companies;

    public boolean checkTradeAvailable(Depot buyer, Depot seller){
        boolean flag;
        BigDecimal productPrice = seller.getTotalCost();
        if (buyer.getCash().compareTo(productPrice) == 1 && buyer.getCash().compareTo(productPrice.add(new BigDecimal("50"))) == 1){
            if(seller.getCash().compareTo(new BigDecimal("100").subtract(productPrice)) == -1){
                if(seller.getNativeCount() > 16 && buyer.getForeignCount() < 40){
                    flag = true;
                } else {
                    System.out.println("Seller and Buyer's Storage is not suitable for trading");
                    flag = false;
                }
            } else {
                System.out.println("Seller's Cash is going to be to much");
                flag = false;
            }
        } else {
            System.out.println("buyers cash is not enough");
            flag = false;
        }
        return flag;
    };

    private void doRandomTrade() {

    }

    /* trade between to depots */
    public void doTrade(Depot buyer, Depot seller) {

        Product tradeProduct = seller.getNativeProduct();
        BigDecimal productPrice = seller.getTotalCost();

        /* 1. buyer's cash reduced */
        buyer.setCash(round(buyer.getCash().subtract(productPrice)));

        /* 2. seller's stock reduced */
        seller.productCountsMap.put(tradeProduct, seller.productCountsMap.get(tradeProduct) - 1);
        seller.setNativeCount(seller.productCountsMap.get(tradeProduct));

        /* 3. buyer's stock increased */
        buyer.productCountsMap.put(tradeProduct, buyer.productCountsMap.get(tradeProduct) + 1);
        buyer.setForeignCount(getForeignProductSum(buyer.getNativeProduct(), buyer.productCountsMap));

        /* 4. seller's cash increased */
        seller.setCash(round(seller.getCash().add(productPrice)));
    }
}
