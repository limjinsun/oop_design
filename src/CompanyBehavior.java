public interface CompanyBehavior {
    public void calculatePurchaseCost();
    public void calculateDeliveryCost();
    public void tradeInfo();
    public void purchaseAmount();

}
